from django.contrib import admin
from posts.models import *


class AdminArea(admin.ModelAdmin):
    ordering = ['order', ]

admin.site.register(Area, AdminArea)
admin.site.register(Post)
admin.site.register(Tag)



