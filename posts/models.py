from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class Area(models.Model):
    name = models.CharField(max_length=32)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=32)

    def __str__(self):
        return self.name


class Post(models.Model):
    title = models.CharField(max_length=256)
    text = models.TextField()
    created = models.DateTimeField(default=timezone.now, blank=False)
    author = models.ForeignKey(User, null=False)
    public = models.BooleanField(default=False, blank=False)
    area = models.ForeignKey(Area, null=False)
    tags = models.ManyToManyField(Tag)

    def __str__(self):
        return self.title
